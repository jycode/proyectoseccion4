/*
                                                Curso: Fundamentos de C++
                            Tema: Proyecto 2 (Comentarios, Variables, Constantes, Función Principal)
                                                   Creado por Jycode
 
                                              Katya Cornejo | Zandor Sánchez
 */

// Diseñar un programa que calcule la densidad poblacional de una isla: habitantes 9,800, superficie 15 km2

#include <iostream>

double calcularDensidadPoblacional(int, double); //Declarar la función

int main() {
    
    int numeroHabitantes = 9800;
    const double superficieIsla = 15;
    
    double densidad = calcularDensidadPoblacional(numeroHabitantes, superficieIsla);
    
    std::cout << densidad << "\n";
    
    return 0;
}

//La superficie debe ser en Km2
double calcularDensidadPoblacional(int numeroHabitantesP, const double superficieIslaP){ //Definir la función
    return numeroHabitantesP / superficieIslaP;
}
